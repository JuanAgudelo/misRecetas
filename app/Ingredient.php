<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    public function classification()
    {
    	return $this->belongsTo('App\classification');
    }

	public function recipes()
	{
	    return $this->belongsToMany('App\Recipe');
	}
}