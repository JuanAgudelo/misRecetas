<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model implements Authenticatable
{
    public function role()
    {
    	return $this->belongsTo('MisRecetas\Role', 'rol_id');
    }
}