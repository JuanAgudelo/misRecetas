<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class classification extends Model
{
    public function ingredients()
    {
    	return $this->hasMany('App\Ingredient');
    }
}
