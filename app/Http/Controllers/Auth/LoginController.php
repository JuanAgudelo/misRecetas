<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'index']);
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login()
    {
        $this->validate(request(), [
            'usuario'   => 'email|required|string',
            'clave'   => 'required|string'
        ]);

        $credentials = array(
            'email' => request()->post('usuario'),
            'password' => request()->post('clave')
        );

        if (Auth::attempt($credentials)) {
            return redirect()->route('home');
        }

        return back()->withErrors(['general' => trans('auth.failed')])
                     ->withInput(request(['usuario']));
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('log');
    }
}
