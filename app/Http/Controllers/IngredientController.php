<?php

namespace App\Http\Controllers;

use App\Ingredient;
use App\Classification;
use Validator;
use Illuminate\Http\Request;

class IngredientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $active = "ingredients";
            return view('ingredients.index', compact('active')); 
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $classifications = classification::all();
            return view('ingredients.create', compact('classifications'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $validation = Validator::make($request->all(), [
                'nombre'  => 'required|unique:ingredients,name',
                'descripcion'  => 'required',
                'imagen'  => 'required|image|mimes:jpeg,png,jpg|max:2048',
                'clasificacion' => 'required|exists:classifications,id'
            ]);

            $error_array = array();
            $success_output = "";

            if ($validation->fails()) {
                foreach ($validation->messages()->getMessages() as $field_name => $message) {
                    $error_array[] = $message;
                }
            } else {
                if($request->hasFile('imagen')) {
                    $file = $request->file('imagen');
                    $fileName = time().$file->getClientOriginalName();
                    $file->move(public_path().'/img/ingredientes', $fileName);
                }

                $ingredient = new Ingredient;
                $ingredient->name = $request->post('nombre');
                $ingredient->description = $request->post('descripcion');
                $ingredient->image = $fileName;
                $ingredient->classification_id = $request->post('clasificacion');
                $ingredient->save();

                $success_output = "Nuevo ingredinate creado";
            }

            return Response()->json([
                'error'   => $error_array,
                'success' => $success_output
            ]);
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $ingredients = Ingredient::all();
            return view('ingredients.list', compact('ingredients'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $ingredient = Ingredient::find($id);
            return view('ingredients.show', compact('ingredient'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $ingredient = Ingredient::find($id);
            $classifications = Classification::all();
            return view('ingredients.edit', compact('ingredient', 'classifications'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $id = $request->post('id');

            $validation = Validator::make($request->all(), [
                'nombre'  => "required|unique:ingredients,name,$id,id",
                'descripcion'  => 'required',
                'imagen'  => 'image|mimes:jpeg,png,jpg|max:2048',
                'clasificacion' => 'required|exists:classifications,id'
            ]);

            $error_array = array();
            $success_output = "";

            if ($validation->fails()) {
                foreach ($validation->messages()->getMessages() as $field_name => $message) {
                    $error_array[] = $message;
                    $result = false;
                }
            } else {
                $ingredient = Ingredient::find($id);

                if($request->hasFile('imagen')) {
                    $file = $request->file('imagen');
                    $fileName = time().$file->getClientOriginalName();
                    $file->move(public_path().'/img/ingredientes', $fileName);
                    if(file_exists(public_path('img/ingredientes/'.$ingredient->image))){
                      unlink(public_path('img/ingredientes/'.$ingredient->image));
                    }
                    $ingredient->image = $fileName;
                }

                $ingredient->name = $request->post('nombre');
                $ingredient->description = $request->post('descripcion');
                $ingredient->classification_id = $request->post('clasificacion');
                $ingredient->save();

                $success_output = "Cambios guardados";
                $result = true;
            }

            return Response()->json([
                'error'   => $error_array,
                'success' => $success_output,
                'id'      => $id
            ]);
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $delete = Ingrediente::find($request->post('id'))->delete();
            return Response()->json($delete);
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }
}
