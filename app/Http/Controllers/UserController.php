<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use Validator;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $active = "users";
            return view('users.index', compact('active'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(auth()->user()->role->name == 'Super administrador') {
            $roles = Role::all();
            return view('users.create', compact('roles'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->user()->role->name == 'Super administrador') {
            $validation = Validator::make($request->all(), [
                'cedula'  => 'required|unique:users,identityCard|numeric',
                'nombre'  => 'required',
                'correo'  => 'required|email|unique:users,email',
                'usuario' => 'required|unique:users,username',
                'rol'     => 'required'
            ]);            

            $error_array = array();
            $success_output = "";

            if ($validation->fails()) {
                foreach ($validation->messages()->getMessages() as $field_name => $message) {
                    $error_array[] = $message;
                }
            } else {
                $user = new User;
                $user->identityCard = $request->post('cedula');
                $user->name = $request->post('nombre');
                $user->email = $request->post('correo');
                $user->username = $request->post('usuario');
                $user->password = bcrypt('misrecetas');
                $user->role_id = $request->post('rol');
                $user->save();

                $success_output = "Nuevo usuario creado";
            }

            return Response()->json([
                'error'   => $error_array,
                'success' => $success_output
            ]);
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list()
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $users = User::all();
            return view('users.list', compact('users'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $user = User::find($id);
            $roles = Role::all();
            return view('users.edit', compact('user', 'roles'));
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $id = $request->post('id');

            if(auth()->user()->role->name == 'Super administrador') {
                $validation = Validator::make($request->all(), [
                    'cedula'  => "required|unique:users,identityCard,$id,id|numeric",
                    'nombre'  => "required",
                    'correo'  => "required|email|unique:users,email,$id,id",
                    'rol'     => 'required|exists:roles,id'
                ]);
            }

            if(auth()->user()->role->name == 'Administrador') {
                $validation = Validator::make($request->all(), [
                    'cedula'  => "required|unique:users,identityCard,$id,id|numeric",
                    'nombre'  => "required",
                    'correo'  => "required|email|unique:users,email,$id,id"
                ]);            
            }

            $error_array = array();
            $success_output = "";

            if ($validation->fails()) {
                foreach ($validation->messages()->getMessages() as $field_name => $message) {
                    $error_array[] = $message;
                    $result = false;
                }
            } else {
                $user = User::find($id);
                $user->identityCard = $request->post('cedula');
                $user->name = $request->post('nombre');
                $user->email = $request->post('correo');
                if(auth()->user()->role->name == 'Super administrador') {
                    $user->role_id = $request->post('rol');
                }
                $user->save();

                $success_output = "Cambios guardados";
                $result = true;
            }

            return Response()->json([
                'error'   => $error_array,
                'success' => $success_output
            ]);
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if(auth()->user()->role->name == 'Super administrador' or auth()->user()->role->name == 'Administrador') {
            $user = User::find($request->post('id'));
            if ($user->role->id != '1') {
                if($user->role->id != $request->post('id')) {
                    $user->delete();
                } else {
                    return 'false';
                }
            } else {
                return 'false';
            }
            return 'true';
        } else {
            return redirect(route('home'))->with('flash', 'No tienes acceso a esta ruta');
        }
    }
}