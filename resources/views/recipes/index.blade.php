@extends('layouts.app')

@section('title', 'Recetas')

@section('content')
	
	@include('layouts.tabs')

	<div class="border border-custom border-top-0 bg-white p-2">
		{{-- tabla  --}}
		<div class="row">
			<div class="col-12 text-center">
				<h1 class="mt-2">Lista de recetas</h1>
				<div class="col-12 mb-3 p-0 text-right">
					<button type="button" class="btn btn-primary" id="btn-add">
						<i class="fas fa-plus mr-1"></i>Nuevo
					</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="table table-responsive">
					<table class="table table-bordered table-hover">
						<thead class="thead-light">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Nombre</th>
								<th scope="col">Descripción</th>
								<th scope="col">Cantidad de ingredientes</th>
								<th scope="col">Acciones</th>
							</tr>
						</thead>
						<tbody id="result">						
							{{-- Mostrar la lista de las recetas mediante AJAX --}}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	{{-- formulario modal --}}
	<div id="modal">
		{{-- Cargar el formulario modal mediante AJAX --}}
	</div>

	{{-- Notificaciones --}}
	<div id="notificaciones"></div>

@endsection