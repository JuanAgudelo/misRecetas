@if(count($recipes) > 0)
	{{ $cont = 1}} 
	@foreach($recipes as $recipe)
		<tr>
			<th scope="row">{{ $cont++ }}</th>
			<td>{{ $recipe->name }}</td>
			<td>{!! (strlen($recipe->description)>35) ? substr($recipe->description,0,35)."..." : $recipe->description !!}</td>
			<td>{{ count($recipe->ingredients) }}</td>
			<td>
				<button type="button" class="btn btn-info" id="btn-show" data-id="{{ $recipe->id }}">Detalles</button>
				<button type="button" class="btn btn-warning" id="btn-edit" data-id="{{ $recipe->id }}">Editar</button>
				<button type="button" class="btn btn-danger" id="btn-delete" data-id="{{ $recipe->id }}" data-token="{{ csrf_token() }}">Eliminar</button>
			</td>
		</tr>
	@endforeach
@else
<tr>
	<td colspan="5">No se han encontrado ingredientes.</td>
</tr>
@endif