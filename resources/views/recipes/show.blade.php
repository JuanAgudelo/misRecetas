<div class="modal" role="dialog" id="modal-action">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Detalles del ingrediente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="card text-center">
					<div class="card-header p-0">
						<div class="row m-0 p-0 rounded-top">
							<div class="col-12 p-4 shadow stripe"></div>
						</div>
						<div class="row">
							<div class="col-12 text-center ">
								<h5 class="mt-3 mb-0 text-primary">Receta</h5>
								<h3 class="font-weight-bold">{{ $recipe->name }}</h3>
							</div>
						</div>
						</div>
					<img class="card-img-top rounded-0" src="{{ asset('img/recetas/'.$recipe->image) }}" alt="Card image cap">
					<div class="card-body p-0">
						<div class="row m-0">
							<div class="col-6 p-2 bg-light">
								<h4 class="font-weight-bold border-bottom">Ingredientes</h4>
								<div class="text-left px-2">
									@foreach($recipe->ingredients as $ingredient)
										<span class="card-text">{{ $ingredient->pivot->quantity.' '.$ingredient->pivot->unit.' '.$ingredient->name }}</span><br>
									@endforeach
								</div>
							</div>
							<div class="col-6 p-2 bg-light text-justify">
								<h4 class="font-weight-bold border-bottom">Descripcion</h4>
								<p class="card-text">{{ $recipe->description }}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				</div>
			</div>
		</div>
	</div>
</div>