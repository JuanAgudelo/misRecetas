<div class="modal" role="dialog" id="modal-action">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="{{ route('receta.store') }}" class="needs-validation form-recipes" id="form" data-action="insert" enctype="multipart/form-data" novalidate>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Nueva receta</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">					
					<div class="col-12 border border-info">
						<div class="row">
							<div class="col-12 p-4 shadow stripe"></div>
						</div>
						<div class="row">
							<div class="col-12 text-center ">
								<h5 class="mt-3 mb-0 text-primary">Receta</h5>
								<h3 class="font-weight-bold" id="titulo">Arroz blanco</h3>
								<input type="text" name="titulo" id="tituloInput" class="form-control hidden">
							</div>
						</div>
						<div class="row my-2">
							<div class="col-12 p-0 container-hover" id="imagen">
								<div class="bg-light d-flex justify-content-center align-items-center w-100 h-100">
									<div class="text-center py-5 my-5">
										<h1><i class="fas fa-image"></i></h1>
										<span>Clic aquí para seleccionar imagen</span>
									</div>
									<!-- <img src="{{ asset('img/recetas/arroz_blanco.jpg') }}" class="img-fluid" alt="Responsive image"> -->
								</div>
								<!-- <div class="d-flex justify-content-center align-items-center w-100 h-100" id="info">
									<div class="bg-primary text-white p-3">
										<h4>
											<i class="fas fa-image"></i>
											<span> Cargar imagen</span>
										</h4>
									</div>
								</div> -->
							</div>
						</div>
						<div class="row" id="detalle">
							<div class="col-6 p-1 border-right border-white">
								<div class="col-12 text-center font-weight-bold">INGREDIENTES</div>
								<a href="" data-toggle="modal" data-target="#exampleModal" data-backdrop="static" data-keyboard="false" class="item-color">
									<div class="col-12 bg-light text-body">
										<div class="text-center py-5">
											<h1><i class="fas fa-plus-circle"></i></h1>
											<span>Clic aquí para agregar ingredientes</span>
										</div>
									</div>
								</a>
							</div>
							<div class="col-6 p-1 border-left border-white">
								<div class="col-12 text-center font-weight-bold">PREPARACIÓN</div>
								<div class="border-top border-white container-hover" id="preparacion" style="height: calc(100% - 22px);">
									<div class="col-12 bg-light text-body h-100 p-0" id="preparacionContenido">
										<div class="text-center py-5">
											<h1><i class="fas fa-plus-circle"></i></h1>
											<span>Clic aquí para agregar la preparación</span>
										</div>
									</div>
									<textarea class="form-control hidden" rows="7" name="preparacion" id="preparacionTextarea"></textarea>
								</div>
							</div>							
						</div>
					</div>

					{{ csrf_field() }}
					<input type="file" name="imagen" id="imagenInput" class="hidden" accept="image/x-png,image/jpg,image/jpeg">

					
					<!-- <div id="alert"></div>
					<div class="form-group">
						<label for="nombre">nombre:</label>
						<input type="text" class="form-control" name="nombre"  id="nombre" placeholder="Por favor ingrese un nombre" required>
						<div class="invalid-feedback">Por favor, ingrese un nombre.</div>
					</div>
					<div class="form-group">
						<label for="descripcion">Descripción:</label>
						<textarea class="form-control" name="descripcion" id="descripcion" rows="3" required></textarea>
						<div class="invalid-feedback">Por favor, ingrese una descripción.</div>
					</div>
					<div class="form-group">
						<label for="imagen">Imagen:</label>
						<input type="file" class="form-control" name="imagen" id="imagen" placeholder="Por favor seleccione una imagen" accept="image/x-png,image/jpg,image/jpeg">
						<div class="invalid-feedback">Por favor, Seleccione una imagen.</div>
					</div> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="action-modal btn btn-primary">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal" id="exampleModal">
	<div class="modal-dialog modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<select class="select-search" name="state">
					<option value="AL">Alabama</option>
					<option value="WY">Wyoming</option>
				</select>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<script>

	/* Recetas. */
// Titulo
$(document).on('click', '#titulo', function() {
	$('#titulo').addClass('hidden');
	$('#tituloInput').removeClass('hidden');
	$('#tituloInput').focus();
});

$(document).on('keypress', '#tituloInput', function(e) {
	if(e.which == 13) {
		$('#tituloInput').focusout();
    }
});

$(document).on('blur', '#tituloInput', function() {
	if($('#tituloInput').val() != '') {
		$('#tituloInput').addClass('hidden');
		$('#titulo').html($('#tituloInput').val());
		$('#titulo').removeClass('hidden');
	} 
});

// Preparacion
$(document).on('click', '#preparacion', function() {
	$('#preparacionContenido').addClass('hidden');
	console.log($('#preparacionTextarea').removeClass('hidden'));
	$('#preparacionTextarea').focus();
	alert();
});

// $(document).on('keypress', '#preparacion', function(e) {
// 	if(e.which == 13) {
// 		$('#preparacionTextarea').focusout();
//     }
// });

$(document).on('blur', '#preparacionTextarea', function() {
	if($('#preparacionTextarea').val() != '') {
		$('#preparacionTextarea').addClass('hidden');
		$('#preparacionContenido').html(
			'<div class="position-absolute w-100" style="">'+
				'<p class="text-justify p-2">'+ $('#preparacionTextarea').val() + '</p>'+
			'</div>'+
			'<div class="d-flex justify-content-center align-items-center w-100 h-100 hover">'+
				'<div class="bg-primary text-white p-3">'+
					'<h5>'+
						'<i class="fas fa-pencil"></i>'+
						'<span>Editar</span>'+
					'</h5>'+
				'</div>'+
			'</div>'
		);
		$('#preparacion div:nth-child(1)').removeClass('hidden bg-light');
	} 
});

$(document).on('click', '#imagen', function() {
	$("#imagenInput").click();
});

$(document).on('change', '#imagenInput', function() {

	var $imagen = URL.createObjectURL(event.target.files[0]);

	$('#imagen div:nth-child(1)').html(
		'<div class="w-100" >'+
			'<img src="'+ $imagen +'" class="img-fluid" alt="Responsive image">'+
		'</div>'+
		'<div class="d-flex justify-content-center align-items-center w-100 h-100 hover">'+
			'<div class="bg-primary text-white p-3">'+
				'<h4>'+
					'<i class="fas fa-image"></i>'+
					'<span>Cambiar imagen</span>'+
				'</h4>'+
			'</div>'+
		'</div>'
	);
});

	/* select search */
$(document).ready(function() {
    $('.select-search').select2();
});
</script>