@if(count($users) > 0)
	{{ $cont = 1 }}
	@foreach($users as $user)
		<tr>
			<th scope="row">{{ $cont++ }}</th>
			<td>{{ $user->identityCard }}</td>
			<td>{{ $user->name }}</td>
			<td>{{ $user->email }}</td>
			<td>{{ $user->username }}</td>
			<td>{{ $user->role->name }}</td>
			<td>
				<button type="button" class="btn btn-warning" id="btn-edit" data-id="{{ $user->id }}">Editar</button>
				<button type="button" class="btn btn-danger" id="btn-delete" data-id="{{ $user->id }}" data-token="{{ csrf_token() }}">Eliminar</button>
			</td>
		</tr>
	@endforeach
@else
<tr>
	<td colspan="7">No se han encontrado usuarios.</td>
</tr>
@endif