<div class="modal" role="dialog" id="modal-action">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="{{ route('usuario.update') }}" class="needs-validation" id="form" data-action="edit" novalidate>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Editar usuario</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<div id="alert"></div>
					<div class="form-group">
						<label for="cedula">Cedula:</label>
						<input type="number" class="form-control number" name="cedula"  id="cedula" placeholder="Por favor ingrese el numero de cedula"  value="{{ $user->identityCard }}" required>
						<div class="invalid-feedback">Por favor, ingrese un numero de cedula válido.</div>
					</div>
					<div class="form-group">
						<label for="nombre">Nombre completo:</label>
						<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Por favor ingrese los nombres y apellidos" value="{{ $user->name }}" required>
						<div class="invalid-feedback">Por favor, ingrese un nombre completo.</div>
					</div>
					<div class="form-group">
						<label for="correo">Correo electrónico:</label>
						<input type="email" autocomplete='email' class="form-control" name="correo" id="correo" placeholder="Por favor ingrese el correo electronico" value="{{ $user->email }}" required>
						<div class="invalid-feedback">Por favor, ingrese un correo válido.</div>
					</div>
					<div class="form-group">
						<label for="usuario">Usuario:</label>
						<input type="text" class="form-control" name="usuario" id="usuario" placeholder="Por favor ingrese un nombre de usuario" value="{{ $user->username }}" disabled>
						<div class="invalid-feedback">Por favor, ingrese un usuario válido.</div>
					</div>
					@if (auth()->user()->role->name == 'Super administrador') 
						<div class="form-group">
							<label for="rol">Rol:</label>
							<select class="form-control" name="rol" id="rol" required>
								@if(count($roles) > 0)
									@foreach($roles as $rol)
										<option value="{{ $rol->id }}" @if ($rol->id == $user->id) selected @endif>{{ $rol->name }}</option>
									@endforeach
								@else
									<option value="">No hay clasificaciones para seleccionar</option>
								@endif
							</select>
						</div>
					@endif
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" value="{{ $user->id }}">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="action-modal btn btn-primary" id="submit">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>