@if(count($classifications) > 0)
	{{ $cont = 1}} 
	@foreach($classifications as $classification)
		<tr>
			<th scope="row">{{ $cont++ }}</th>
			<td>{{ $classification->name }}</td>
			<td>
				<button type="button" class="btn btn-warning" id="btn-edit" data-id="{{ $classification->id }}">Editar</button>
				<button type="button" class="btn btn-danger" id="btn-delete" data-id="{{ $classification->id }}" data-token="{{ csrf_token() }}">Eliminar</button>
			</td>
		</tr>
	@endforeach
@else
<tr>
	<td colspan="3">No se han encontrado clasificaciones.</td>
</tr>
@endif