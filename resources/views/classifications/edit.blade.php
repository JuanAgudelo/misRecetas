<div class="modal" role="dialog" id="modal-action">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" class="needs-validation" id="form" data-action="edit" novalidate>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Editar clasificación</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<div id="alert"></div>
					<div class="form-group">
						<label for="clasificacion">Nombre:</label>
						<input type="text" class="form-control" name="clasificacion" id="clasificacion" placeholder="Por favor ingrese la clasificación" value="{{ $classification->name }}" required>
						<div class="invalid-feedback">Por favor, ingrese una clasificación.</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" value="{{ $classification->id }}">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="action-modal btn btn-primary" id="submit">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>