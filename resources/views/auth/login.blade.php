@extends('layouts.app')

@section('title', 'login')

@section('content')
	
	<div class="row d-flex justify-content-center mt-3">
		<div class="col-md-6 col-md-offset-3">
			<div class="card">
				<div class="card-header text-center" >
					<h1 class="card-title">Acceso a la aplicación</h1>
				</div>
				<div class="card-body">
					<form method="POST" action="{{ route('login') }}">
						{{ csrf_field() }}
						<div class="form-group">
							{!! $errors->first('general', '<span class="form-text text-danger">:message</span>') !!}
						</div>
						<div class="form-group hast-error">
							<label for="usuario">Correo electronico:</label>
							<input type="text" class="form-control {{ $errors->has('usuario') ? 'is-invalid' : '' }}" name="usuario" id="usuario" placeholder="Ingrese su correo." value="{{ old('usuario') }}">
							{!! $errors->first('usuario', '<small id="emailHelp" class="form-text text-danger">:message</small>') !!}
						</div>
						<div class="form-group">
							<label for="clave">Clave</label>
							<input type="password" class="form-control {{ $errors->has('usuario') ? 'is-invalid' : '' }}" name="clave" id="clave" placeholder="Ingrese su clave."}">
							{!! $errors->first('clave', '<small id="emailHelp" class="form-text text-danger">:message</small>') !!}
						</div>
						<button type="submit" class="btn btn-primary btn-lg btn-block">Acceder</button>
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection