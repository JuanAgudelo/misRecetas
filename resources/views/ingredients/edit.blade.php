<div class="modal" role="dialog" id="modal-action">
	<div class="modal-dialog">
		<div class="modal-content">
			<form method="POST" action="{{ route('ingrediente.update') }}" class="needs-validation" id="form" data-action="edit" enctype="multipart/form-data" novalidate>
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Editar ingrediente</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					{{ csrf_field() }}
					<div id="alert"></div>
					<div class="form-group">
						<label for="nombre">nombre:</label>
						<input type="text" class="form-control" name="nombre"  id="nombre" placeholder="Por favor ingrese un nombre" value="{{ $ingredient->name }}" required>
						<div class="invalid-feedback">Por favor, ingrese un nombre.</div>
					</div>
					<div class="form-group">
						<label for="descripcion">Descripción:</label>
						<textarea class="form-control" name="descripcion" id="descripcion" rows="3" required>{{ $ingredient->description }}</textarea>
						<div class="invalid-feedback">Por favor, ingrese una descripción.</div>
					</div>
					<div class="form-group">
						<label for="imagen">Imagen:</label>
						<input type="file" class="form-control" name="imagen" id="imagen" placeholder="Por favor seleccione una imagen" value="{{ $ingredient->image }}">
						<div class="invalid-feedback">Por favor, Seleccione una imagen.</div>
					</div>
					<div class="form-group">
						<label for="clasificacion">Clasificacion:</label>
						<select class="form-control select-search" name="clasificacion" id="clasificacion" required>
							@if(count($classifications) > 0)
								<option value="">Seleccione una clasificación</option>
								@foreach($classifications as $classification)
									<option value="{{ $classification->id }}" @if($classification->id == $ingredient->classification->id) selected @endif>{{ $classification->name }}</option>
								@endforeach
							@else
								<option value="">No hay clasificaciones para seleccionar</option>
							@endif
						</select>
						<div class="invalid-feedback">Por favor, Seleccione una clasificacion.</div>
					</div>
				</div>
				<div class="modal-footer">
					<input type="hidden" name="id" value="{{ $ingredient->id }}">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button type="submit" class="action-modal btn btn-primary" id="submit">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>