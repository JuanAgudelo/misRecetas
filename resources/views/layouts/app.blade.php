<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <title>Mis recetas - @yield('title')</title>
		
		<!-- Bootstrap -->
    	<link rel="stylesheet" href="{{ asset('css/bootstrap-4.1.0.min.css') }}">
		<!-- icon -->
        <link rel="stylesheet" href="{{ asset('css/fontawesome-5.0.13.min.css') }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Custom style -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <!-- Select2 -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	</head>
	<body>
		<header>
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark px-5">
				<a class="navbar-brand" href="#">Mis Recetas</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						@if (Auth::guest())
							<li class="nav-item active">
								<a class="nav-link" href="{{ route('register') }}">Registrate</a>
							</li>
							<li class="nav-item active">
								<a class="nav-link" href="{{ route('log') }}">Iniciar sesión</a>
							</li>
						@else
						<li class="nav-item dropdown ">
							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								{{ auth()->user()->name }}
							</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<div class="text-center">
									<h5>Tipo de usuario:</h5>
									<span>{{ auth()->user()->role->name }}</span>
								</div>
								<div class="dropdown-divider"></div>
								<div>
									<form method="POST" action="{{ route('logout') }}">
										{{ csrf_field() }}
										<button class="btn btn-danger btn-xs mx-4"> Cerrar sesión</button>
									</form>
								</div>
							</div>
						</li>
						@endif
					</ul>
				</div>
			</nav>
		</header>

		<div class="container">	
			@if (session()->has('flash'))
				<div class="alert alert-info my-2" role="alert">{{ session('flash') }}</div>
			@endif
			@yield('content')
		</div>

		<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
		<script src="{{ asset('js/bootstrap-4.1.0.min.js') }}"></script>
		<script src="{{ asset('js/popper-1.14.0.min.js') }}"></script>
		<script src="{{ asset('js/notificaciones.js') }}"></script>
		<script src="{{ asset('js/app.js') }}"></script>
		<!-- Select2 -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		
	</body>
</html>