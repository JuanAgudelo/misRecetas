@extends('layouts.app')

@section('title', 'Inicio')

@section('content')

	@include('layouts.tabs')
	
	<div class="border border-custom border-top-0 bg-white p-2">
		<div class="row d-flex justify-content-center mt-3">
			<div class="col-md-6 col-md-offset-3">
				<div class="card">
					<div class="card-header text-center" >
						<h3 class="card-title">Bienvenido {{ auth()->user()->name }}</h3>
					</div>
					<div class="card-body">
						Correo: {{ auth()->user()->email }} <br>
						Usuario: {{ auth()->user()->username }}
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection