<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = new User();
        $user->identityCard = '0000000000';
        $user->name = "Super administrador";
        $user->email = 'superadministrador@misrecetas.com';
        $user->username = "SuperAdmin";
        $user->password = bcrypt('misrecetas');
        $user->role_id = 1;
        $user->save();

        $user = new User();
        $user->identityCard = '0000000001';
        $user->name = "Administrador";
        $user->email = 'administrador@misrecetas.com';
        $user->username = "Admin";
        $user->password = bcrypt('misrecetas');
        $user->role_id = 2;
        $user->save();

        $user = new User();
        $user->identityCard = '0000000002';
        $user->name = "Usuario";
        $user->email = 'usuario@misrecetas.com';
        $user->username = "usuario";
        $user->password = bcrypt('misrecetas');
        $user->role_id = 3;
        $user->save();
    }
}
