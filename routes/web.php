<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Rutas de login
Route::get('/', 'Auth\LoginController@index')->name('log');
Route::get('/login', 'Auth\LoginController@index')->name('log');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

// Rutas de registro
Route::get('/registro', 'Auth\RegisterController@index')->name('register');
Route::post('/registro', 'Auth\RegisterController@register')->name('register');

// Ruta home
Route::get('/inicio', 'AppController@index')->name('home');

// Rutas de usuario
Route::group([],function () {
	Route::get('/usuarios', 'UserController@index')->name('usuario.index');
	Route::get('/usuarios/create', 'UserController@create')->name('usuario.create');
	Route::post('/usuarios/create', 'UserController@store')->name('usuario.store');
	Route::get('/usuarios/list', 'UserController@list')->name('usuario.list');
	//Route::get('/usuario/{usuario}', 'UserController@list')->name('usuario.show');
	Route::get('usuarios/edit/{id}', 'UserController@edit')->name('usuario.edit');
	Route::post('/usuarios/edit', 'UserController@update')->name('usuario.update');
	Route::post('/usuarios/delete', 'UserController@destroy')->name('usuario.destroy');
});

// Rutas de clasificaciones
Route::group([],function () {
	Route::get('/clasificaciones', 'ClassificationController@index')->name('clasificacion.index');
	Route::get('/clasificaciones/create', 'ClassificationController@create')->name('clasificacion.create');
	Route::post('/clasificaciones/create', 'ClassificationController@store')->name('clasificacion.store');
	Route::get('/clasificaciones/list', 'ClassificationController@list')->name('clasificacion.list');
	//Route::get('/usuario/{usuario}', 'ClassificationController@')->name('usuario.show');
	Route::get('/clasificaciones/edit/{id}', 'ClassificationController@edit')->name('clasificacion.edit');
	Route::post('/clasificaciones/edit', 'ClassificationController@update')->name('clasificacion.update');
	Route::post('/clasificaciones/delete', 'ClassificationController@destroy')->name('clasificacion.destroy');
});

// Rutas de ingredientes
Route::group([],function () {
	Route::get('/ingredientes', 'IngredientController@index')->name('ingrediente.index');
	Route::get('/ingredientes/create', 'IngredientController@create')->name('ingrediente.create');
	Route::post('/ingredientes/create', 'IngredientController@store')->name('ingrediente.store');
	Route::get('/ingredientes/list', 'IngredientController@list')->name('ingrediente.list');
	Route::get('/ingredientes/show/{id}', 'IngredientController@show')->name('ingrediente.show');
	Route::get('/ingredientes/edit/{id}', 'IngredientController@edit')->name('ingrediente.edit');
	Route::post('/ingredientes/edit', 'IngredientController@update')->name('ingrediente.update');
	Route::post('/ingredientes/delete', 'IngredientController@destroy')->name('ingrediente.destroy');
});

// Rutas de recetas
Route::group([],function () {
	Route::get('/recetas', 'RecipeController@index')->name('receta.index');
	Route::get('/recetas/create', 'RecipeController@create')->name('receta.create');
	Route::post('/recetas/create', 'RecipeController@store')->name('receta.store');
	Route::get('/recetas/list', 'RecipeController@list')->name('receta.list');
	Route::get('/recetas/show/{id}', 'RecipeController@show')->name('receta.show');
	Route::get('/recetas/edit/{id}', 'RecipeController@edit')->name('receta.edit');
	Route::post('/recetas/edit', 'RecipeController@update')->name('receta.update');
	Route::post('/recetas/delete', 'RecipeController@destroy')->name('receta.destroy');
});
