var pathname = window.location.pathname;

$(document).ready(function() {
	if (pathname!='/login' && pathname!='/registro' && pathname!='/inicio') {
		list();
	}

	$('#openBtn').click(function() {
		$('#myModal').modal({
			show: true
		})
	});

	// Multiples ventanas modales
	$(document).on({
		'show.bs.modal': function() {
			var zIndex = 1040 + (10 * $('.modal:visible').length);
			$(this).css('z-index', zIndex);
			setTimeout(function() {
				$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
			}, 0);
		},
		'hidden.bs.modal': function() {
			if ($('.modal:visible').length > 0) {
        setTimeout(function() {
        	$(document.body).addClass('modal-open');
        }, 0);
    }
}
}, '.modal');

	// Evitar el ingreso de la letra 'e'.
	$('.number').keypress(function(tecla) {
		if(tecla.charCode == 101) {
			tecla.preventDefault();
		};
	})
});


/* Open modal create. */
$(document).on('click', '#btn-add', function() {
	$.get(pathname + '/create', function(data) {
		$('#modal').html(data);
		$('#modal-action').modal({backdrop: 'static', keyboard: false});
		$('#modal-action').modal('show');
		$('.select-search').select2();
	});
});

/* Open modal edit. */
$(document).on('click', '#btn-edit', function() {
	var id = $(this).data('id');
	$.get(pathname + '/edit/' + id, function(data) {
		$('#modal').html(data);
		$('#modal-action').modal({backdrop: 'static', keyboard: false});
		$('#modal-action').modal('show');
		$('.select-search').select2();
	});
});

/* Create and update. */
$(document).on('click','#submit', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
        	event.preventDefault();
        	var action = $('#form').data('action');
        	var attr = $('#form').attr('enctype');

        	if (typeof attr !== typeof undefined && attr !== false) {
        		var data = new FormData( $('#form')[0] );
        		if(action == 'insert') {
	        		var url = pathname + '/create';
	        		saveFile(url, data, action);
	        	} else if (action == 'edit') {
	        		var url = pathname + '/edit';
	        		saveFile(url, data, action);
	        	}
        	} else {
        		var data = $('#form').serialize();
        		if(action == 'insert') {
	        		var url = pathname + '/create';
	        		save(url, data, action);
	        	} else if (action == 'edit') {
	        		var url = pathname + '/edit';
	        		save(url, data, action);
	        	}
        	}     	
        }
        form.classList.add('was-validated');
      }, false);
    });
});


/* Read. */
$(document).on('click', '#btn-show', function() {
	var id = $(this).data('id');
	$.get(pathname + '/show/' + id, function(result) {
		$('#modal').html(result);
		$('#modal-action').modal('show');
	});
});

/* Delete. */
$(document).on('click', '#btn-delete', function() {
	var id = $(this).data('id');
	var token = $(this).data('token');
	var option = confirm("¿desea continuar?");
	if (option == true) {
        $.post(pathname + '/delete', {_token: token, id:id}, function(result) {
			if(result == 'true') {
				list();
				$.notificaciones({ 
					mensaje : 'El registro ha sido eliminado',
					width: 300,
					cssClass : 'success',
					timeout : 2000,
					fadeout : 1000,
					radius : 10
				});
			} else if(result == 'false') {
				$.notificaciones({ 
					mensaje : 'El registro no puede ser eliminado',
					width: 300,
					cssClass : 'error',
					timeout : 2000,
					fadeout : 1000,
					radius : 10
				});
			} else {
				list();
				$.notificaciones({ 
					mensaje : 'El registro ha sido eliminado',
					width: 300,
					cssClass : 'success',
					timeout : 2000,
					fadeout : 1000,
					radius : 10
				});
			}
		});
	} 
});

/* listar usurios. */
function list() {
	$('#result').html(
		'<tr>' +
		'<td colspan="7" class="text-center"><i class="fa fa-spinner fa-spin fa-3x"></i></td>' +
		'</tr>'
	);
	$.get(pathname + '/list', function(data) {
			$('#result').html(data);
	})
	.fail(function() {
		$('#result').html(
			'<tr>' +
			'<td colspan="7" class="text-center text-danger">Ha ocurrido un error, por favor actualice la pagina.</td>' +
			'</tr>'
		);
  	});
}

/* Guardar formulario. */
function save(url, data, action) {
	$.post(url, data, function(data){
		console.log(data.error);
		if(data.error.length > 0) {
			var error_html = '';
			error_html += '<div class="alert alert-danger"> Corrige los siguientes errores';
			for (var i = 0; i < data.error.length; i++) {
				error_html += '<li>' + data.error[i] + '</li>';
				console.log("error " + i );
			}

			error_html += '</div>';
			$('#alert').html(error_html);
		} else {
			$('#form')[0].reset();
			$('#modal-action').modal('hide');
			list();
			if (action == 'insert') {
				$.notificaciones({ 
					mensaje : 'El registro se ha guardado',
					width: 300,
					cssClass : 'success',
					timeout : 2000,
					fadeout : 1000,
					radius : 10
				});
			} else if(action == 'edit') {
				$.notificaciones({ 
					mensaje : 'los cambios se han guardado',
					width: 300,
					cssClass : 'success',
					timeout : 2000,
					fadeout : 1000,
					radius : 10
				});
			} else {
				$.notificaciones({ 
					mensaje : 'No se pudo realizar la acción',
					width: 300,
					cssClass : 'error',
					timeout : 2000,
					fadeout : 1000,
					radius : 10
				});
			}
		}
	});
}

/* Guardar formulario con archivos. */
function saveFile (url, data, action) {
	$.ajax({
		url : url,
		type : "POST",
		data: data,
		contentType: false,
		processData: false,
		success : function(data) {
			console.log(data.error);
			if(data.error.length > 0) {
				var error_html = '';
				error_html += '<div class="alert alert-danger"> Corrige los siguientes errores';
				for (var i = 0; i < data.error.length; i++) {
					error_html += '<li>' + data.error[i] + '</li>';
					console.log("error " + i );
				}

				error_html += '</div>';
				$('#alert').html(error_html);
			} else {
				$('#form')[0].reset();
				$('#modal-action').modal('hide');
				list();
				if (action == 'insert') {
					$.notificaciones({ 
						mensaje : 'El registro se ha guardado',
						width: 300,
						cssClass : 'success',
						timeout : 2000,
						fadeout : 1000,
						radius : 10
					});
				} else if(action == 'edit') {
					$.notificaciones({ 
						mensaje : 'los cambios se han guardado',
						width: 300,
						cssClass : 'success',
						timeout : 2000,
						fadeout : 1000,
						radius : 10
					});
				} else {
					$.notificaciones({ 
						mensaje : 'No se pudo realizar la acción',
						width: 300,
						cssClass : 'error',
						timeout : 2000,
						fadeout : 1000,
						radius : 10
					});
				}
			}
		}
	});
}

/* Agregar input en recetas. */
$(document).on('click', '#addInputs', function() {

	var html = '';
	var id = $('#selectIngredient').val();
	var nombre = $('#selectIngredient option:selected').text();

	html += '<tr>';
	html += '<td>';
	html += '<span>' + nombre + '</span>';
	html += '<input type="hidden" class="form-control" name="idIng[]" value="' + id + '">';
	html += '</td>';
	html += '<td>';
	html += '<input type="number" class="form-control" min="1" name="cantidad[]" required>';
	html += '<div class="invalid-feedback">Por favor, ingrese una cantidad.</div>';
	html += '<td>';
	html += '<input type="text" class="form-control" name="unidad[]" required>';
	html += '<div class="invalid-feedback">Por favor, ingrese una unidad.</div>';
	html += '</td>';
	html += '<td>';
	html += '<button type="button" class="btn btn-danger btn-block removeInput">X</button>';
	html += '</td>';
	html += '</tr>';

	if (id != '') {
		$('#itemTable').append(html);
		$('#selectIngredient option:selected').remove();
	} else {
		$('#selectIngredient').focus();
	}
	
});

/* Eliminar input en recetas. */
$(document).on('click', '.removeInput', function() {
	$(this).closest('tr').remove();
});